# Development
### Requirements
- NodeJS 
- VSCode

### Steps
1. Run `npm install`.
2. Open this folder in VSCode.
3. Modify the config files:  
3.1. Open config.json.  
3.1.1 Set the `"apiKey"` to yout Open Tok API Key.    
3.2. Open testConfig.json.    
3.2.1 Set the `"secret"` to yout Open Tok Secret.
4. Run the "Test" configuration to make sure your enviroment is setup correctly.
4. Run the "Run" configuration.