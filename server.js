const log4js = require('log4js');
const WebSocketServer = require('websocket').server;
const http = require('http');
const config = require('./config.js');
const SessionRecorder = require('./session-recorder.js');
require('http-shutdown').extend();

const LISTENER_ENDPOINT = '/listener';
const RECORDER_ENDPOINT = '/recorder';
const SESSIONID_QUERY_PARAM = "sessionId"
const RECORDERTOKEN_QUERY_PARAM = "recorderToken"

const logger = log4js.getLogger();

class Server {
    constructor() {
        this._listenerConnectionForSession = {};
        this._recorderForSession = {};
        this._recorderConnectionForSession = {};

        this._address = `${config.hostname}:${config.port}`;
        this._pageUrl = `http://${this._address}`;
        this._listenerUrlTemplate = `ws://${this._address}${LISTENER_ENDPOINT}?${SESSIONID_QUERY_PARAM}=%s&${RECORDERTOKEN_QUERY_PARAM}=%s`;
        this._recorderUrlTemplate = `ws://${this._address}${RECORDER_ENDPOINT}?${SESSIONID_QUERY_PARAM}=%s`;
    }

    start() {
        this._httpServer = http.createServer((request, response) => {
            response.writeHead(200, {'Content-Type': 'text/html'});        
            response.write("OpenTok Headless Subscriber Server.");
            response.end();
        }).withShutdown();
        
        this._httpServer.listen(config.port);
        
        this._socketServer = new WebSocketServer({
            httpServer: this._httpServer,
            maxReceivedFrameSize: config.maxReceivedFrameSize,
            maxReceivedMessageSize: config.maxReceivedMessageSize
        });
        
        this._socketServer.on('request', async request => {
            switch(request.resourceURL.pathname) {
        
                case LISTENER_ENDPOINT: 
                    var sessionId = request.resourceURL.query[SESSIONID_QUERY_PARAM];
                    var recorderToken = request.resourceURL.query[RECORDERTOKEN_QUERY_PARAM];
                    if(sessionId && recorderToken) {
                        // Start listening for any data published for a session.
                        var connection = request.accept(null, request.origin);
                        await this._setupListener(sessionId, recorderToken, connection);
                    } else {
                        request.reject(400);
                    }            
                    break;
        
                case RECORDER_ENDPOINT:
                    var sessionId = request.resourceURL.query[SESSIONID_QUERY_PARAM];
                    if(sessionId) {
                        // Recorded data to be forwarded to listeners for the session.
                        var connection = request.accept(null, request.origin);
                        this._setupRecorderForwarding(sessionId, connection);
                    } else {
                        request.reject(400);
                    }        
                    break;
        
                default: 
                    request.reject(404);
            }
        });
    }

    async shutdown() {
        return new Promise(resolve => {
            this._socketServer.shutDown();
            this._httpServer.shutdown(resolve);
        });
    }

    async _setupListener(sessionId, recorderToken, connection) {
        this._listenerConnectionForSession[sessionId] = connection;
    
        const self = this;
        this._listenerConnectionForSession[sessionId].on('message', message => {
            logger.warn(`Received message for Session Id: "${sessionId}". Session Listeners aren\'t supposed to send messages.`);
        });
    
        this._listenerConnectionForSession[sessionId].on('close', async (reasonCode, description) => {            
            // There is a chance a new connection may be made while this one is being closed, hence the removeTime flag incase it has
            // already been replaced by the time the browser has finished disposing
            const removeTime = Date.now();
            
            self._recorderForSession[sessionId].removeTime = removeTime;
            self._listenerConnectionForSession[sessionId].removeTime = removeTime;

            await self._recorderForSession[sessionId].dispose();
            
            if(self._recorderForSession[sessionId].removeTime === removeTime) {
                delete self._recorderForSession[sessionId];
            }

            if(self._listenerConnectionForSession[sessionId].removeTime === removeTime) {            
                delete self._listenerConnectionForSession[sessionId];
            }
        
            logger.debug(`Closed Listener Connection for Session Id: "${sessionId}". Reason Code: "${reasonCode}". Description: "${description}".`);
        });
    
        this._recorderForSession[sessionId] = new SessionRecorder(
            config.apiKey, 
            sessionId, 
            recorderToken, 
            this._pageUrl, 
            this._recorderUrlTemplate, 
            error => this._onError(sessionId, error),
            config.recordInterval,
            config.subscriber,
            config.debug);

        await this._recorderForSession[sessionId].start();

        logger.debug(`Setup Listener Connection for Session Id: "${sessionId}".`);
    }
    
    _setupRecorderForwarding(sessionId, connection) {
        this._recorderConnectionForSession[sessionId] = connection;

        const self = this;
        this._recorderConnectionForSession[sessionId].on('message', message => {            
            if (message.type === 'binary') {                
                self._listenerConnectionForSession[sessionId].send(message.binaryData);
    
                logger.debug(`Forwarded Binary Message for Session Id: "${sessionId}" of ${message.binaryData.length} bytes.`);
    
            } else {
                logger.warn(`Received unhandled message type: "${message.type}" for Session Id: "${sessionId}".`);
            }
        });
    
        this._recorderConnectionForSession[sessionId].on('close', function(reasonCode, description) {
            delete self._recorderConnectionForSession[sessionId];
    
            logger.debug(`Closed Recorder Connection for Session Id: "${sessionId}". Reason Code: "${reasonCode}". Description: "${description}".`);
        });
    };

    _onError(sessionId, error) {
        const connection = this._listenerConnectionForSession[sessionId];
        connection.send(JSON.stringify(error));
    }
}

module.exports = Server;