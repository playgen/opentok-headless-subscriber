const testConfig = require('./testConfig.js');
const config = require('../config.js');
const OpenTok = require('opentok');
const puppeteer = require('puppeteer');

function create () {
    new OpenTok(config.apiKey, testConfig.secret);
}
   
async function generateSessionAndRecorderToken() {
    const openTok = new OpenTok(config.apiKey, testConfig.secret);
    const session =  await generateSession(openTok);
    const recorderToken = generateRecorderToken(session);                

    return [session.sessionId, recorderToken];
}

function generateSession (openTok) {    
    return new Promise((resolve, reject) => {
        openTok.createSession({mediaMode: "relayed"}, (error, session) => {
            if(error) {
                reject(error);
            } else {
                resolve(session);
            }
        });
    });
};

async function createSessionAndPublishers(pageUrl, publisherCount) {
    const browser = await puppeteer.launch({
        //headless: true,
        args: ['--use-fake-ui-for-media-stream']
    });
            
    const openTok = new OpenTok(config.apiKey, testConfig.secret);
    const session = await generateSession(openTok);

    const publishers = [];
    var i = 0;
    while(i < publisherCount) {
        const [publisherPage, connectionData] = await setupPublisher(session, config.apiKey, browser, pageUrl, i);
        publishers.push([publisherPage, connectionData]);
        i++;
    }
    
    return [browser, session, publishers];
}

function generateRecorderToken(session, data) {
    return session.generateToken({
        role: "subscriber",
        data: 'clientId:recorder'
    });
}

async function setupPublisher(session, apiKey, browser, pageUrl, publisherId) { 
    const connectionData = `clientId:publisher-${publisherId}`;
    const token = session.generateToken({
        role: 'publisher',
        data: connectionData
    });
    
    const page = await browser.newPage();
    
    await page.goto(pageUrl, {waitUntil: "networkidle0"}); // Hack to gain permission to access local storage

    await page.addScriptTag({url: 'https://static.opentok.com/v2/js/opentok.min.js'});        
           
    await page.evaluate((apiKey, sessionId, token) => {
        window.session = OT.initSession(apiKey, sessionId);                    
    
        window.session.on("streamCreated", event => {
            window.session.subscribe(event.stream, "subscriber", {
                width: "100%",
                height: "100%"
            });
        });

        window.session.connect(token, () => {
            window.session.publish(OT.initPublisher("publisher"));
            Promise.resolve();
        });
    }, apiKey, session.sessionId, token);

    return [page, connectionData];
}

module.exports = {
    createSessionAndPublishers,
    generateRecorderToken,
    generateSessionAndRecorderToken
};