const opentokHelpers = require('./opentok-helpers.js');
const Server = require('../server.js');

describe("OpenTok", () => {
    var server;

    beforeEach(() => {
       server = new Server();
       server.start();
    });

    afterEach(async () => {
        await server.shutdown();
    });

    it("Can launch two publisher session", async () => {        
        // Act
        var [browser, sessionId, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl)

        // Cleanup
        await browser.close();
    });
});