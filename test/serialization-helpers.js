const BSON = require('bson');

function deserializeUInt16(bytes) {
    const byteLength = 8;
    var number = 0;

    for(var i in bytes) {
        number += bytes[i];

        if(i < bytes.length - 1) {
            number = number << byteLength;
        };
    };

    return number;
};

function deserializeRecordings (serializedRecordings) {    
    const dataArray = Object.values(serializedRecordings);
    const bson = new BSON();

    const recordings = [];

    var index = 0;
    while(index < dataArray.length - 1) {    
        const serializedMetadataLength = dataArray.slice(index, index + 2);
        index += 2;
        
        const metadataLength = deserializeUInt16(serializedMetadataLength);                   
        
        const serializedMetadata = dataArray.slice(index, index + metadataLength);
        index += metadataLength;

        const metadata = bson.deserialize(new Buffer(serializedMetadata));
        
        const serializedBlob = dataArray.slice(index, index + metadata.blobLength);
        index += metadata.blobLength;
        
        recordings.push({
            metadata: metadata,
            blob: serializedBlob
        });
    }

    return recordings;
}

module.exports = {
    deserializeUInt16,
    deserializeRecordings
}