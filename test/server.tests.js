const assert = require('assert');
const WebSocketClient = require('websocket').client;
const Promise = require('promise');
const Server = require('../server.js');
const util = require('util');
const opentokHelpers = require('./opentok-helpers.js');
const serializationHelpers = require('./serialization-helpers.js');
const fs = require('fs');
const path = require('path');
const os = require('os');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');

describe("Server", () => {
    var server;

    beforeEach(() => {
       server = new Server();
       server.start();
    });

    afterEach(async () => {
        await server.shutdown();
    });

    it("Can open Listener Connection", async () => {        
        // Arrange
        const client = new WebSocketClient();                
        const [sessionId, recorderToken] = await opentokHelpers.generateSessionAndRecorderToken();
        
        // Act            
        await new Promise((resolve, reject) => {
            client.on('connectFailed', error => reject);

            client.on('connect', connection => {
                connection.on('error', reject);

                connection.on('close', resolve);

                connection.close();
            });

            client.connect(util.format(server._listenerUrlTemplate, sessionId, recorderToken));
        });
    });

    it("Can open Recorder Connection", async () => {
        // Arrange
        const listenerClient = new WebSocketClient();
        const recorderClient = new WebSocketClient();
        const [sessionId, recorderToken] = await opentokHelpers.generateSessionAndRecorderToken();
        
        const listenerConnection = await new Promise((resolve, reject) => {
            listenerClient.on('connectFailed', reject);

            listenerClient.on('connect', connection => {
                connection.on('error', reject);               

                resolve(connection);
            });

            listenerClient.connect(util.format(server._listenerUrlTemplate, sessionId, recorderToken));
        });

        // Act - if the promise is rejected this test will fail
        const recorderConnection = await new Promise((resolve, reject) => {
            recorderClient.on('connectFailed', error => reject);

            recorderClient.on('connect', connection => {
                connection.on('error', reject);

                resolve(connection);
            });

            recorderClient.connect(util.format(server._recorderUrlTemplate, sessionId));
        });
    });

    it("Recorder Conneciton data is forwared to the Listener Connection", async () => {
        // Arrange
        const listenerClient = new WebSocketClient();
        const recorderClient = new WebSocketClient();
        const [sessionId, recorderToken] = await opentokHelpers.generateSessionAndRecorderToken();
        
        const listenerConnection = await new Promise((resolve, reject) => {
            listenerClient.on('connectFailed', reject);

            listenerClient.on('connect', connection => {
                connection.on('error', reject);               

                resolve(connection);
            });

            listenerClient.connect(util.format(server._listenerUrlTemplate, sessionId, recorderToken));
        });

        const recorderConnection = await new Promise((resolve, reject) => {
            recorderClient.on('connectFailed', error => reject);

            recorderClient.on('connect', connection => {
                connection.on('error', reject);

                resolve(connection);
            });

            recorderClient.connect(util.format(server._recorderUrlTemplate, sessionId));      
        });

        const receiveMessageTask = new Promise((resolve, reject) => {
            listenerConnection.on('error', reject);
            listenerConnection.on('message', resolve);        
        });

        const id = "2b7aaaeb-e346-4cea-8e09-e9cbb891a52d";
        const blob = "I am some data";
                
        const serializedId = Buffer.from(id, 'utf8');
        const serializedBlob = Buffer.from(blob, 'utf8');;
        
        var buffer = new Buffer(1 + serializedId.length + serializedBlob.length);
        buffer[0] = serializedId.length;
        serializedId.copy(buffer, 1);
        serializedBlob.copy(buffer, 1 + serializedId.length);

        // Act                    
        recorderConnection.send(buffer);        

        const receivedMessage = await receiveMessageTask;

        // Assert
        assert.ok(receivedMessage);
        assert.equal(receivedMessage.type, 'binary');

        const receivedBinaryData = receivedMessage.binaryData;
        const idLength = receivedBinaryData[0];

        const receivedId = receivedBinaryData.toString('utf8', 1, 1 + idLength);
        assert.equal(receivedId, id);

        const deserializedBlob = receivedBinaryData.toString('utf8', 1 + idLength);
        assert.equal(deserializedBlob, blob);
    });

    it("Can get listener data for OpenTok session", async () => {
        // Arrange
        var [browser, session, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl, 2);
        const recorderToken = opentokHelpers.generateRecorderToken(session);                

        const client = new WebSocketClient();
        
        const connection = await new Promise((resolve, reject) => {
            client.on('connectFailed', reject);

            client.on('connect', connection => {
                connection.on('error', reject);               

                resolve(connection);
            });

            client.connect(util.format(server._listenerUrlTemplate, session.sessionId, recorderToken));
        });        

        // Act
        const recordings = await new Promise((resolve, reject) => {
            const messageData = [];

            connection.on('message', message => {
                if (message.type === 'binary') {                
                    messageData.push(message.binaryData)
                    if(messageData.length === 3) {
                        resolve(messageData);                    
                    }
                } else {
                    reject(`Received unhandled message type: "${message.type}" for Session Id: "${sessionId}".`);
                }
            });
        });
        
        // Cleanup
        await browser.close();
    
        // Assert
        recordings.forEach(recording => assert.ok);
    });

    it("Can decode received recordings", async () => {
        // Arrange
        const recordingCount = 3;
        const outDir = 'temp/Can decode received recordings';        
        
        if(fs.existsSync(outDir)) {
            rimraf.sync(outDir);
        }

        mkdirp.sync(outDir);
        
        var [browser, session, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl, 2);
        const recorderToken = opentokHelpers.generateRecorderToken(session);                

        const client = new WebSocketClient();
        
        const connection = await new Promise((resolve, reject) => {
            client.on('connectFailed', reject);

            client.on('connect', connection => {
                connection.on('error', reject);               

                resolve(connection);
            });

            client.connect(util.format(server._listenerUrlTemplate, session.sessionId, recorderToken));
        });        

        const serialziedRecordingMessages = await new Promise((resolve, reject) => {
            const messageData = [];

            connection.on('message', message => {
                if (message.type === 'binary') {                
                    messageData.push(message.binaryData)
                    if(messageData.length === recordingCount) {
                        resolve(messageData);
                    }
                } else {
                    reject(JSON.parse(message.utf8Data));
                }
            });
        });

        // Assert
        const format = 'webm'       
        const recordingCounts = [];

        for(var i in serialziedRecordingMessages) {
            const serializedRecordings = serialziedRecordingMessages[i];
            assert.ok(serializedRecordings);

            const recordings = serializationHelpers.deserializeRecordings(serializedRecordings);

            recordingCounts.push(recordings.length);

            for(var j in recordings) {
                const recording = recordings[j];                

                assert.ok(recording.metadata.connectionData);

                const matchingIndex = publishers.findIndex(([publisherPage, connectionData]) => connectionData === recording.metadata.connectionData);
                assert.ok(matchingIndex >= 0);

                assert.equal(recording.metadata.mimeType, `video/${format}`);

                const fileName = path.join(outDir, `${recording.metadata.streamId}_${i}.${format}`);
                
                fs.writeFileSync(fileName, new Buffer(recording.blob));

                assert.ok(fs.existsSync(fileName));
            }
        }

        // Should generally have a recording for each publisher
        var sum = 0;
        recordingCounts.forEach(c => sum += c);
        const average = sum / recordingCounts.length;
        assert.ok(average > publishers.length * 0.75, 'Should generally have one recording per publisher.');
        
        if(os.platform() === 'win32') {
            require('child_process').exec(`explorer.exe "${path.normalize(outDir)}"`);
        }
        
        // Cleanup
        await browser.close();
    });
});