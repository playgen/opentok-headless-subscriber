const SessionRecorder = require('../session-recorder.js')
const assert = require('assert');
const puppeteer = require('puppeteer');
const Server = require('../server.js');
const opentokHelpers = require('./opentok-helpers.js');
const serializationHelpers = require('./serialization-helpers.js');
const config = require('../config.js');
const BSON = require('bson');

describe('Session Recorder', async () => {
    var server;

    beforeEach(() => {
       server = new Server();
       server.start();
    });

    afterEach(async () => {
        await server.shutdown();
    });
   
    it('Recorder can make websocket connection', async () => {
        // Arrange
        const sessionId = "test-session-id";
        const recorder = new SessionRecorder(null, sessionId, null, server._pageUrl, server._recorderUrlTemplate, error => {throw error;});
        await recorder._setupBrowser();
        
        // Act
        await recorder._setupWebSocket(); // This will be rejected and throw an error if it fails
    });    

    it('Can serialize metadata', async () => {
        // Arrange
        const recorder = new SessionRecorder(null, null, null, server._pageUrl, null, error => {throw error;});
        await recorder._setupBrowser();

        const metadata = {
            streamId: "11111111-1111-1111-1111-111111111111",
            connectionData: "Some connection data",
            blobLengths: 1170,
            recordingStarted: 100,
            recordingEnded: 100,
            additional: "I'm a little teapot."
        };

        // Act
        const serializedMetadata = await recorder._page.evaluate(metadata => {            
            const serializedMetadata = window.sessionRecorder.serializeMetadata(metadata);            
            return Promise.resolve(serializedMetadata);            
        }, metadata);
        
        // Assert
        assert.ok(serializedMetadata);

        const bson = new BSON();
        const array = Object.values(serializedMetadata);
        const deserializedMetadata = bson.deserialize(new Buffer(array));

        assert.ok(deserializedMetadata);
        for(var key in metadata) {
            assert.equal(deserializedMetadata[key], metadata[key]);
        }        
    });

    it('Can serialize a blob', async () => {
        // Arrange
        const recorder = new SessionRecorder(null, null, null, server._pageUrl, null, error => {throw error;});
        await recorder._setupBrowser();

        const blobPart = "I am some more data";

        // Act
        const serializedBlob = await recorder._page.evaluate(async blobPart => {
            const blob = new Blob([blobPart], {type : 'text/plain'});
            const serializedBlob = await window.sessionRecorder.serializeBlob(blob);            
            return Promise.resolve(serializedBlob);
        }, blobPart);
        
        // Assert
        assert.ok(serializedBlob);
        const array = Object.values(serializedBlob);
        const decodedBlobPart = new Buffer(array).toString();
        assert.equal(decodedBlobPart, blobPart);
    });

    it('Can serialize a single blob', async () => {
        // Arrange                
        const recorder = new SessionRecorder(null, null, null, server._pageUrl, null, error => {throw error;});
        await recorder._setupBrowser();
        
        const testData = [{
            streamId: "11111111-1111-1111-1111-111111111111",
            blobPart: "I am some blob content"
        }];

        // Act
        const serializedData = await recorder._page.evaluate(async (testData) => {            
            const recordings = [];

            testData.forEach(item => recordings.push({
               metadata: {streamId: item.streamId}, 
               blob: new Blob([item.blobPart], {type : 'text/plain'})
            }));

            const serializedData = await window.sessionRecorder.serializeRecordings(recordings);            

            return Promise.resolve(serializedData);
        }, testData);
        
       
        // Assert
        assert.ok(serializedData);

        const recordings = serializationHelpers.deserializeRecordings(serializedData);

        assert.equal(recordings.length, testData.length);

        testData.forEach(testItem => {     
            const recording = recordings.find(r => r.metadata.streamId == testItem.streamId);
            assert.ok(recording);                                   

            const decodedBlobPart = new Buffer(recording.blob).toString();
            assert.equal(decodedBlobPart, testItem.blobPart);                        
        });
    });

    it('Can serialize multiple blobs', async () => {
        // Arrange                
        const recorder = new SessionRecorder(null, null, null, server._pageUrl);
        await recorder._setupBrowser();
        
        const testData = [{
            streamId: "11111111-1111-1111-1111-111111111111",
            blobPart: "I am some blob content"
        }, {
            streamId: "22222222-2222-2222-2222-222222222222",
            blobPart: "I am some other blob content"
        }];

        // Act
        const serializedData = await recorder._page.evaluate(async (testData) => {            
            const recordings = [];

            testData.forEach(item => recordings.push({
                metadata: {streamId: item.streamId}, 
                blob: new Blob([item.blobPart], {type : 'text/plain'})
            }));

            const serializedData = await window.sessionRecorder.serializeRecordings(recordings);            

            return Promise.resolve(serializedData);
        }, testData);
        
        // Assert
        assert.ok(serializedData);

        const recordings = serializationHelpers.deserializeRecordings(serializedData);

        assert.equal(recordings.length, testData.length);

        testData.forEach(testItem => {     
            const recording = recordings.find(r => r.metadata.streamId == testItem.streamId);
            assert.ok(recording);                                   

            const decodedBlobPart = new Buffer(recording.blob).toString();
            assert.equal(decodedBlobPart, testItem.blobPart);                        
        });
    });
    
    it('Can handle stream destroyed', async () => {
        // Arrange                                
        var [browser, session, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl, 1);
        const recorderToken = opentokHelpers.generateRecorderToken(session);                
        const recorder = new SessionRecorder(config.apiKey, session.sessionId, recorderToken, server._pageUrl, server._recorderUrlTemplate, error => {throw error;});

        const connectionTask = new Promise(resolve => {
            // Overwrite the request handler setup in the server
            server._socketServer._events.request = request => {
                var connection = request.accept(null, request.origin);
                resolve(connection);
            };
        });

        await recorder.start();
        
        const connection = await connectionTask;
        
        const data = await new Promise((resolve, reject) => {
            connection.on('message', message => {
                if (message.type === 'binary') {                
                    resolve(message.binaryData);                    
                } else {
                    reject(`Received unhandled message type: "${message.type}" for Session Id: "${sessionId}".`);
                }
            });
        });

        // Act
        await browser.close();
        await new Promise(resolve => setTimeout(resolve, 1000));

        // Cleanup        
        await recorder.dispose();

        // Assert
        assert.ok(data);
        assert.ok(!recorder.error)
    });

    it('Can record single publisher data', async () => {
        // Arrange                                
        var [browser, session, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl, 1);
        const recorderToken = opentokHelpers.generateRecorderToken(session);        
        const recorder = new SessionRecorder(config.apiKey, session.sessionId, recorderToken, server._pageUrl, server._recorderUrlTemplate, error => {throw error;});

        const connectionTask = new Promise(resolve => {
            // Overwrite the request handler setup in the server
            server._socketServer._events.request = request => {
                var connection = request.accept(null, request.origin);
                resolve(connection);
            };
        });

        // Act
        await recorder.start();
        
        const connection = await connectionTask;
        
        const recordings = await new Promise((resolve, reject) => {
            const messageData = [];

            connection.on('message', message => {
                if (message.type === 'binary') {                
                    messageData.push(message.binaryData)
                    if(messageData.length === 3) {
                        resolve(messageData);                    
                    }
                } else {
                    reject(`Received unhandled message type: "${message.type}" for Session Id: "${sessionId}".`);
                }
            });
        });

        // Cleanup
        await browser.close();
        await recorder.dispose();

        // Assert
        recordings.forEach(recording => assert.ok)
        assert.ok(!recorder.error)
    });

    it('Can record multiple publisher data', async () => {
        // Arrange                                
        var [browser, session, publishers] = await opentokHelpers.createSessionAndPublishers(server._pageUrl, 2);
        const recorderToken = opentokHelpers.generateRecorderToken(session);        
        const recorder = new SessionRecorder(config.apiKey, session.sessionId, recorderToken, server._pageUrl, server._recorderUrlTemplate, error => {throw error;});

        const connectionTask = new Promise(resolve => {
            // Overwrite the request handler setup in the server
            server._socketServer._events.request = request => {
                var connection = request.accept(null, request.origin);
                resolve(connection);
            };
        });

        // Act
        await recorder.start();
        
        const connection = await connectionTask;
        
        const recordings = await new Promise((resolve, reject) => {
            const messageData = [];

            connection.on('message', message => {
                if (message.type === 'binary') {                
                    messageData.push(message.binaryData)
                    if(messageData.length === 3) {
                        resolve(messageData);                    
                    }
                } else {
                    reject(`Received unhandled message type: "${message.type}" for Session Id: "${sessionId}".`);
                }
            });
        });

        // Cleanup
        await browser.close();
        await recorder.dispose();

        // Assert
        recordings.forEach(recording => assert.ok)
        assert.ok(!recorder.error)
    });
});