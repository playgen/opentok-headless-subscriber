const fs = require('fs');

const data = fs.readFileSync(__dirname + '/testConfig.json', 'utf8');
const config = JSON.parse(data);

module.exports = config;