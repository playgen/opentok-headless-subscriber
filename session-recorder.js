const log4js = require('log4js');
const puppeteer = require('puppeteer');
const util = require('util');

const logger = log4js.getLogger();

class SessionRecorder {
    constructor(apiKey, sessionId, token, pageUrl, recorderUrlTemplate, onError, pollInterval, subscriberConfig, debug) {
        this._apiKey = apiKey;
        this._sessionId = sessionId;
        this._token = token;
        this._pageUrl = pageUrl;
        this._recorderUrl = util.format(recorderUrlTemplate, sessionId);
        this._onErrorCallback = onError;
        this._pollInterval = pollInterval;
        this._subscriberConfig = subscriberConfig;        
        this._debug = debug;
        this._browser;
        this._page;
        this._error;
        this._isDisposing = false;        
        this._isDisposed = false;
    }

    get error () {
        return this._error;
    }

    async start () {
        await this._setupBrowser();  
        await this._setupBroadcast();      
        await this._setupOpenTokRecording();
    }

    async dispose () {
        if(this._isDisposed || this._isDisposing) {
            return;
        }

        this._isDisposing = true;

        if(this._browser) {
            await this._browser.close();
        }

        this._isDisposed = true;
    }

    async _setupOpenTokRecording() {                   
        await this._page.addScriptTag({url: 'https://static.opentok.com/v2/js/opentok.min.js'});     

        await this._createOpenTokSession();
        await this._setupOpenTokRecorder();        
        await this._setupRecordedDataPoller();
        await this._setupOnRecorderBlob();
        await this._connectOpenTok();
    }

    async _setupBroadcast() {
        await this._setupWebSocket();
        await this._addBroadcast();
    }

    async _addBroadcast() {
        await this._page.evaluate((broadcastUrl, debug) => {
            window.sessionRecorder.broadcast = async recordings => {           
                if(debug) {
                    recordings[0].metadata.debugInfo.recording.push(["preSerialization", Date.now()]);
                }

                const serializedRecordings = await window.sessionRecorder.serializeRecordings(recordings);                
                window.sessionRecorder.webSocket.send(serializedRecordings.buffer);
            };

            Promise.resolve();
        }, this._broadcastUrl, this._debug);
    }

    async _createOpenTokSession() {
        await this._page.evaluate((apiKey, sessionId) => {
            window.sessionRecorder.session = OT.initSession(apiKey, sessionId);
            return Promise.resolve();
        }, this._apiKey, this._sessionId);
    }

    async _connectOpenTok() {
        await this._page.evaluate(token => new Promise(resolve => window.sessionRecorder.session.connect(token, resolve)), 
            this._token);
    }

    async _setupOnRecorderBlob() {
        await this._page.evaluate(debug => {
            window.sessionRecorder.onRecorderBlob = (streamId, requestTime, blob) => {      
                var debugInfo = debug ? { recording: [["dataAvailable", Date.now() ]]} : null;

                var dataRequest;
                var dataRequestIndex;
                for(dataRequestIndex = window.sessionRecorder.dataRequests.length - 1; dataRequestIndex >= 0; dataRequestIndex--) {
                    const checkDataRequest = window.sessionRecorder.dataRequests[dataRequestIndex];
                    if(checkDataRequest.before <= requestTime && requestTime <= checkDataRequest.after) {
                        dataRequest = checkDataRequest;
                        break;
                    }
                }

                if(!dataRequest) {
                    if(window.sessionRecorder.recordingConfigs[streamId].ended) {
                        console.debug(`Recorder for streamId: "${streamId}" has been ended and will be ignored.`);
                    } else {
                        console.warn(`Couldn't find a matching DataRequest for streamId: "${streamId}" at TimeCode: ${requestTime}.`);
                    }
                    return;
                }                

                dataRequest.blobs[streamId] = blob;
                dataRequest.received++;

                if(dataRequest.received === dataRequest.requested) {
                    window.sessionRecorder.dataRequests.splice(dataRequestIndex, 1);

                    const recordings = [];
                    Object.entries(dataRequest.metadatas).forEach(([streamId, metadata]) => {
                        const recordingConfig = window.sessionRecorder.recordingConfigs[streamId];
                        metadata.streamId = streamId;
                        metadata.connectionData = recordingConfig.connectionData;
                        metadata.mimeType = recordingConfig.mimeType;

                        const recording = {
                            metadata: metadata,                            
                            blob: dataRequest.blobs[streamId]                            
                        };                        

                        if(debug) {
                            metadata.debugInfo = debugInfo;
                        }

                        if(recording.blob instanceof Blob) {                            
                            recordings.push(recording);
                        } else {
                            console.warn(`Ignoring invalid blob for ${metadata.streamId}. Started: ${metadata.started}, Ended: ${metadata.ended}.`);
                        }
                    });

                    window.sessionRecorder.broadcast(recordings);
                };
            };            
        }, this._debug);
    }

    async _setupRecordedDataPoller() {
        await this._page.evaluate(pollInterval => {
            window.sessionRecorder.dataRequests = [];

            window.sessionRecorder.requestData = () => {
                const dataRequest = {
                    requested: 0,
                    received: 0,
                    before: Date.now(),
                    after: null,
                    metadatas: {},
                    blobs: {},
                };

                Object.entries(window.sessionRecorder.recordingConfigs).forEach(([streamId, recordingConfig]) => {
                    const metadata = {
                        started: recordingConfig.started,
                        ended: Date.now(),
                    };                        

                    recordingConfig.started = metadata.ended;
                    dataRequest.metadatas[streamId] = metadata;

                    if(recordingConfig.recorder && recordingConfig.recorder.state === 'recording') {
                        recordingConfig.recorder.stop(); 
                        delete recordingConfig.recorder;                       
                        dataRequest.requested++;
                    }   
                                        
                    if(recordingConfig.rtcStream.active) {
                        window.sessionRecorder.startRecorder(streamId, recordingConfig.rtcStream);
                    }
                });

                dataRequest.after = Date.now();
                window.sessionRecorder.dataRequests.push(dataRequest);

                window.sessionRecorder.pollTimer = setTimeout(window.sessionRecorder.requestData, pollInterval);
            };
            
            window.sessionRecorder.pollTimer = setTimeout(window.sessionRecorder.requestData, pollInterval);
                        
            return Promise.resolve();
        }, this._pollInterval);
    }

    async _setupOpenTokRecorder() {
        await this._page.evaluate(subscriberConfig => {          
            window.sessionRecorder.recordingConfigs = {};    
            
            window.sessionRecorder.startRecorder = (streamId, rtcStream) => {      
                const recording = window.sessionRecorder.recordingConfigs[streamId];
                if(recording.recorder) {
                    console.warn(`There is already a recorder for: ${streamId}.`);
                }

                const recorder = new MediaRecorder(rtcStream, {mimeType: recording.mimeType});
                recorder.ondataavailable = event => {
                    const blob = event.data;
                    const requestTime = Math.trunc(event.timecode);
                    window.sessionRecorder.onRecorderBlob(streamId, requestTime, blob);
                };
                recorder.start();    

                window.sessionRecorder.recordingConfigs[streamId].started = Date.now();
                window.sessionRecorder.recordingConfigs[streamId].recorder = recorder;
            };                  

            // tok can't connect if you dont initially subscribe to the video stream
            const subscriberConfigFix = Object.assign({}, subscriberConfig, {subscribeToVideo: true})

            const mimeType = subscriberConfig.subscribeToVideo ? 'video/webm' : 'audio/webm';

            window.sessionRecorder.session.on("streamCreated", event => {                 
                var subscriber = window.sessionRecorder.session.subscribe(
                    event.stream, 
                    "subscriber", 
                    subscriberConfigFix,
                    error => {
                        if(error) {                        
                            console.error(error);
                            return;
                        }                                        
                        
                        if(!subscriberConfig.subscribeToVideo) {
                            subscriber.subscribeToVideo(false);
                        }
                        
                        const streamId = event.stream.id;                        
                                                
                        window.sessionRecorder.recordingConfigs[streamId] = {                        
                            rtcStream: OT.subscribers.find(s => s.streamId == streamId)._.webRtcStream(),
                            connectionData: event.stream.connection.data,
                            mimeType: mimeType
                        };

                        console.debug('Added recording for streamId: "' + streamId + '".');
                });  
                
                subscriber.on("videoEnabled", function(event) {
                    subscriber.subscribeToVideo(false);
                });
            });

            window.sessionRecorder.session.on("streamDestroyed", event => {          
                const streamId = event.stream.id;
                const recordingConfig = window.sessionRecorder.recordingConfigs[streamId];                

                if(recordingConfig) {                    
                    recordingConfig.ended = Date.now();

                    if(recordingConfig.recorder) {
                        recordingConfig.recorder.stop(); // Implicitly calls requireData - the system will currently ignore this last blob
                    }

                    console.debug('Stopped recording for streamId: "' + streamId + '".');
                };
            });

            return Promise.resolve();
        }, this._subscriberConfig);
    }

    async _setupBrowser(pollInterval) {
        this._browser = await puppeteer.launch({
            args: ["--no-sandbox"],            
            //headless: false
        });

        this._page = await this._browser.newPage();
        
        this._page.on('error', this._onPageError.bind(this));
        this._page.on('pageerror', this._onPageError.bind(this));
        this._page.on('console', message => {
            var type = message.type();
            var text = message.text();
            
            if(type === 'warning') type = 'warn';

            if(logger.hasOwnProperty(type) && typeof console[type] === 'function') {
                logger[type](text);
            } else {
                logger.warn(`Received message type: "${type}" with text: "${text}".`);
            }            
        });

        await this._page.goto(this._pageUrl, {waitUntil: "networkidle0"});
        await this._addSessionRecorderContext();        
        await this._addBrowserFunctions();        
    }

    _onPageError (error) {        
        this._error = error;
        logger.error(error);

        if(this._onErrorCallback) {
            this._onErrorCallback(error);
        }
    }

    async _addSessionRecorderContext() {
        await this._page.evaluate(() => window.sessionRecorder = {});
    }

    async _setupWebSocket() {
        await this._page.evaluate(url => {
            return new Promise((resolve, reject) => {
                var didOpen = false;
                window.sessionRecorder.webSocket = new WebSocket(url);
                window.sessionRecorder.webSocket.binaryType = 'arraybuffer';

                window.sessionRecorder.webSocket.onopen = () => {
                    didOpen = true;
                    resolve();
                };

                window.sessionRecorder.webSocket.onerror = event => console.error(event);
                window.sessionRecorder.webSocket.onclose = event => {
                    console.debug(`Closed WebSocket Connection. Code: ${event.code} Reason: ${event.reason}.`);
                    if(!didOpen) {
                        reject(event);
                    }
                };

                window.sessionRecorder.webSocket.onmessage = message => {
                    console.warn('Session Recorder is not meant to receive messages.');
                };
            });
        }, this._recorderUrl);
    }

    async _addBrowserFunctions() {
        await this._addSerializeBlob();
        await this._addSerializeMetadata();
        await this._addSerializeAsUInt16();
        await this._addSerializeRecordings();        
    }

    async _addSerializeRecordings() {
        await this._page.evaluate(async () => {            
            window.sessionRecorder.serializeRecordings = async recordings => {
                const serializeBlobPromises = [];

                recordings.forEach(recording => 
                    serializeBlobPromises.push(window.sessionRecorder.serializeBlob(recording.blob)
                        .then(serializedBlob => recording.serializedBlob = serializedBlob))
                );
                
                await Promise.all(serializeBlobPromises);

                var bufferSize = 0;
                var addToBuffer = [];
                recordings.forEach(recording => {
                    recording.metadata.blobLength = recording.serializedBlob.byteLength;
                    
                    const serializedMetadata = window.sessionRecorder.serializeMetadata(recording.metadata);
                    const serializedMetadataLength = window.sessionRecorder.serializeAsUInt16(serializedMetadata.byteLength);

                    addToBuffer.push(serializedMetadataLength, serializedMetadata, recording.serializedBlob);
                    bufferSize += serializedMetadataLength.byteLength + serializedMetadata.byteLength + recording.serializedBlob.byteLength;
                });

                const buffer = new Uint8Array(bufferSize);                
                var offset = 0;
                addToBuffer.forEach(item => {
                    buffer.set(item, offset);
                    offset += item.byteLength;
                });

                return buffer;
            };
        });
    }

    async _addSerializeMetadata () {
        await this._page.addScriptTag({url: 'https://cdnjs.cloudflare.com/ajax/libs/js-bson/1.0.4/bson.min.js'});        

        return this._page.evaluate(() => {           
            window.sessionRecorder.bson = new BSON();
            window.sessionRecorder.serializeMetadata = metadata => window.sessionRecorder.bson.serialize(metadata);
        });
    };

    async _addSerializeAsUInt16 () {
        await this._page.evaluate(() => {           
            window.sessionRecorder.serializeAsUInt16 = number => {
                const byteSize = 8;
                const allUInt8Bytes = 255;
                var bytes = new Uint8Array(16 / byteSize);

                for(var i = bytes.length - 1; i >= 0; i--) {
                    bytes[i] = number & allUInt8Bytes;
                    number = number >> byteSize;
                };
            
                return bytes;
            };
        });
    };

    async _addSerializeBlob () {
        await this._page.evaluate(() => {
            window.sessionRecorder.serializeBlob = (blob) => new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onloadend = event => resolve(new Uint8Array(event.target.result));
                reader.onerror = reject;
                reader.readAsArrayBuffer(blob);
            });
        });
    }
}

module.exports = SessionRecorder;