const log4js = require('log4js');
const Server = require('./server.js');

log4js.configure({
    appenders: { 
        console: { type: 'console' },
        app: { type: 'file', filename: 'logs/app.log' },
    },
    categories: { default: { appenders: ['app','console'], level: 'debug'}}
});
const logger = log4js.getLogger();

const server = new Server();
server.start();

logger.info(`Started Server at: ${server._address}`);